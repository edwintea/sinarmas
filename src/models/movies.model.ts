import {Entity, model, property} from '@loopback/repository';

@model({settings: {strict: false}})
export class Movies extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  title: string;

  @property({
    type: 'string',
  })
  description?: string;

  @property({
    type: 'number',
  })
  duration?: number;

  @property({
    type: 'string',
    required: true,
  })
  artists: string;

  @property({
    type: 'string',
    required: true,
  })
  genres: string;

  @property({
    type: 'string',
    required: true,
  })
  watch_url: string;

  @property({
    type: 'number',
    default: 0,
  })
  viewed?: number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Movies>) {
    super(data);
  }
}

export interface MoviesRelations {
  // describe navigational properties here
}

export type MoviesWithRelations = Movies & MoviesRelations;
