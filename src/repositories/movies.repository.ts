import {DefaultCrudRepository} from '@loopback/repository';
import {Movies, MoviesRelations} from '../models';
import {SinarmasDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class MoviesRepository extends DefaultCrudRepository<
  Movies,
  typeof Movies.prototype.id,
  MoviesRelations
> {
  constructor(
    @inject('datasources.sinarmas') dataSource: SinarmasDataSource,
  ) {
    super(Movies, dataSource);
  }
}
